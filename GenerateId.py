import  math, time, netifaces, os

class init:

    NODE_ID_BITS = 10 # math.pow(2,10) = 1024 # nyumber of servers
    max_node_id = math.pow(2, NODE_ID_BITS) - 1 # 0 to 1023
    nodeId = -1

    def __init__(self):
        self.nodeId = self.createNodeId()
    
    def createNodeId(self):
        mac_addr_hash = self.getMacAddressHash()
        return int(self.max_node_id) & mac_addr_hash

    def getNodeId(self):
        return self.nodeId
    
    def getMacAddressHash(self):
        macString = ""
        interfaces = netifaces.interfaces()
        for interface in interfaces:
            macString = macString + netifaces.ifaddresses(interface)[netifaces.AF_LINK][0]['addr']
        return hash(macString)

class GenerateSequence:
    TOTAL_BITS = 64
    EPOCH_BITS = 42 # with 42 bits, we arw covered till Wed May 15 2109
    NODE_ID_BITS = 10 # math.pow(2,10) = 1024 # nyumber of servers
    SEQUENCE_BITS = 12 # max value math.pow(2, 12) # 4096
    max_sequence = math.pow(2, SEQUENCE_BITS) - 1 # 0 to 4095
    last_timestamp = -1
    nodeId = ""
    sequence = 0

    def __init__(self, node_id):
        self.nodeId = node_id
        # pass

    def waitNextMilliSecond(self, current_time):
        while(current_time == self.last_timestamp):
            current_time = int(time.time() * 1000)
        return current_time

    def generateUniqueIds(self):
        current_timestamp = int(time.time() * 1000)
        if(current_timestamp < self.last_timestamp):
            raise Exception("Something wrong with the timestamps.")
        
        if (current_timestamp == self.last_timestamp):
            self.sequence = self.sequence + 1
            if self.sequence == 10000:
                self.waitNextMilliSecond(current_timestamp)

        else: 
            self.sequence = 0

        self.last_timestamp = current_timestamp
        id = current_timestamp << (self.TOTAL_BITS - self.EPOCH_BITS)
        id |= (self.nodeId << (self.TOTAL_BITS - self.EPOCH_BITS - self.NODE_ID_BITS))
        id |= self.sequence
        return id

# seq = GenerateSequence()
# custom_dict = {}
# x = int(round(time.time() * 1000))

# for i in range(100000):
#     val = seq.generateUniqueIds()
#     custom_dict[val] = True


# assert len(custom_dict) == 100000
# print("total time >>>", len(custom_dict), int(round(time.time() * 1000)) - x)





    
