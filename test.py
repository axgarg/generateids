import time
from GenerateId import init, GenerateSequence

node_id = init().getNodeId()
seq = GenerateSequence(node_id)
custom_dict = {}
t1 = int(round(time.time() * 1000))
# print(t1)

for x in range(100000):
    val = seq.generateUniqueIds()
    custom_dict[val] = True

assert len(custom_dict) == 100000
t2 = int(round(time.time() * 1000))
print("total time >>>", t2-t1)

