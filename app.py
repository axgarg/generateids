from flask import Flask
from GenerateId import GenerateSequence, init
app = Flask(__name__)

node_id = init().getNodeId()
seq = GenerateSequence(node_id)

@app.route('/')
def getUniqueId():
    unique_id = seq.generateUniqueIds()
    return str(unique_id)

if __name__ == '__main__':
    app.run()